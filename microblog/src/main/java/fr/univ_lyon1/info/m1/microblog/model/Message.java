package fr.univ_lyon1.info.m1.microblog.model;

import java.util.Date;
import java.lang.System;

/**
 * Message and its own data.
 */
public class Message {
    private String content;
    private Date date;

    public String getContent() {
        return content;
    }

    public Date getDate(){
        return date;
    }

    /**
     * Build a Message object from it's (String) content.
     */
    public Message(final String content) {
        this.content = content;
        this.date = new Date();
        System.out.println(date);
    }

}
